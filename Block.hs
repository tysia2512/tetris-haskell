{-
Podstawowy moduł, implementujący ruch, wygląd i położenie
pojedynczego bloku.
-}
module Block ( Block,
               create,
               move,
               display,
               shiftDown,
               shiftRight,
               shiftLeft,
               shiftDownN,
               isMovingDown,
               gridPosition ) where
import Graphics.UI.GLUT
import Grid

{-
pole movement: pierwsza para opisuje prędkość po OX i OY,
druga para jest docelowymi współrzędnymi na siatce = gridPosition
-}
data Block = Block { color :: Color3 GLfloat,
                     gridPosition :: (Int, Int),
                     realPosition :: (Float, Float), 
                     movement :: Maybe ((Float, Float), (Int, Int)) } deriving Show

--tworzy nieruchomy blok
create :: Color3 GLfloat -> (Int, Int) -> Block
create color (x, y) =
  Block { Block.color = color,
          gridPosition = (x, y),
          realPosition = Grid.displayedPosition (x, y),
          movement = Nothing }

{-
funkcja pomocnicza - sprawdza, czy klocek nie przekroczył docelowej
współrzędnej, wykonuje ruch z daną prędkością (zmienia lub nie daną współrzędną)
-}
moveAxis :: Float -> Float -> Float -> (Float, Bool)
moveAxis speed a b =
  case (speed > 0.0) of
    True  | (a + speed) < b -> (a + speed, False)
    False | (a + speed) > b -> (a + speed, False)
    _ -> (b, True)

--aktualizuje funkcję klocka co tik (używa pola movement)
move :: Block -> Block
move Block { Block.color = color,
             gridPosition = gridPosition,
             realPosition = (x, y),
             movement = m@(Just ((xSpeed, ySpeed), (xDestination, yDestination))) } =
  if xFinished && yFinished
  then Block { Block.color = color,
               gridPosition = gridPosition,
               realPosition = (xDest, yDest),
               movement = Nothing }
  else Block { Block.color = color,
               gridPosition = gridPosition,
               realPosition = (x', y'),
               movement = m }
  where
  --displayedPosition bierze współrzędne na siatce i zwraca współrzędne do wyświetlenia
  (xDest, yDest)  = displayedPosition (xDestination, yDestination)
  (x', xFinished) = moveAxis xSpeed x xDest
  (y', yFinished) = moveAxis ySpeed y yDest

move static = static

convert :: Float -> GLfloat
convert = fromRational . toRational
  
display :: Block -> IO ()
display Block { Block.color = color,
                gridPosition = gridPosition,
                realPosition = (x, y),
                movement = _movement } =
  renderPrimitive Quads $ do
  Graphics.UI.GLUT.color $ color
  vertex3 (convert x, convert y, 0.0)
  vertex3 (convert (x + xEdgeLength), convert y, 0.0)
  vertex3 (convert (x + xEdgeLength), convert (y + yEdgeLength), 0.0)
  vertex3 (convert x, convert (y + yEdgeLength), 0.0)

shift :: Block -> (Int, Int) -> (Float, Float) -> Block
shift Block { Block.color = color,
              gridPosition = (gx, gy),
              realPosition = realPosition,
              movement = movement }
  (xShift, yShift) (xSpeed, ySpeed) =
  case movement of
    Nothing ->
      Block { Block.color = color,
              gridPosition = (gx + xShift, gy + yShift),
              realPosition = realPosition,
              movement = Just ((xSpeed, ySpeed), (gx + xShift, gy + yShift)) }
    Just ((xSpeed', ySpeed'), (xDest, yDest)) ->
      Block { Block.color = color,
              gridPosition = (gx + xShift, gy + yShift),
              realPosition = realPosition,
              movement = Just ((xSpeed + xSpeed', ySpeed + ySpeed'),
                               (xDest + xShift, yDest + yShift)) }

shiftDown :: Block -> Float -> Block
shiftDown block speed = Block.shift block (0, -1) (0.0, -speed)

shiftLeft :: Block -> Float -> Block
shiftLeft block speed = Block.shift block (-1, 0) (-speed, 0.0)

shiftRight :: Block -> Float -> Block
shiftRight block speed = Block.shift block (1, 0) (speed, 0.0)

--przesuwa w dół wiele razy
shiftDownN :: Block -> Float -> Int -> Block
shiftDownN block speed 0 = block
shiftDownN block speed 1 = shiftDown block speed 
shiftDownN block speed n =
  shiftDown
  (foldl
   (\block _i -> shiftDown block 0.0)
   block
   [1..(n-1)])
  speed

isMovingDown :: Block -> Bool
isMovingDown Block {movement = movement} =
  case movement of
    Just ((_, s), _) | s < 0.0 -> True
    _                          -> False
