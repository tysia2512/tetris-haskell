module Board ( Board,
               Board.empty,
               nonEmptyBlocks,
               isEmpty,
               isCorrect,
               addTetromino,
               clearRows,
               Board.move)
where

import Data.HashMap.Strict

import Block
import Tetromino
import Speed

data Board = Board { board :: HashMap (Int, Int) (Maybe Block) }

empty :: Board
empty =
  let board = foldl
              (\ht k -> insert k Nothing ht)
              Data.HashMap.Strict.empty
              [(x, y) | x <- [0..9], y <- [0..21]]
  in
    Board { board = board }
  
isEmpty :: Board -> (Int, Int) -> Bool
isEmpty Board { board = board } coordinates =
  case Data.HashMap.Strict.lookup coordinates board of
    Just Nothing -> True
    _ -> False

nonEmptyBlocks :: Board -> [Block]
nonEmptyBlocks Board { board = board } =
  Prelude.map
  (\(_, (Just b)) -> b)
  (Prelude.filter
   (\(_, b) -> case b of
                 Nothing -> False
                 _ -> True)
   (toList board))

--sprawdza czy tetromino nie nachodzi na żaden blok na planszy
isCorrect :: Board -> Tetromino -> Bool
isCorrect Board {board = board} tetromino =
  foldl
  (\is b ->
      case Data.HashMap.Strict.lookup (Block.gridPosition b) board of
        Just Nothing -> is 
        _ -> False)
  True
  (Tetromino.blocks tetromino)

--dodaje bloki tetromina do bloków planszy
addTetromino :: Board -> Tetromino -> Board
addTetromino Board {board = board} tetromino = 
  Board {board = board'}
  where
    board' = foldl
             (\board block ->
                Data.HashMap.Strict.insert
               (Block.gridPosition block)
               (Just block)
               board)
             board
             (Tetromino.blocks tetromino)

--usuwa pełne rzędy i przesuwa i nadaje prędkość tym powyżej
--pomocnicza dla clearRows, nie przeindeksowuje bloków
deleteRows :: Board -> Board
deleteRows Board {board = board} =
  Board {board = board'}
  where
    isFull board y =
      foldl
      (\is x ->
         case Data.HashMap.Strict.lookup (x, y) board of
           Just Nothing -> False
           _ -> is
      )
      True
      [0..9]

    adjustRow board y f =
      (foldl
        (\b x -> Data.HashMap.Strict.adjust f (x, y) b)
        board
        [0..9]
      )

    (board', _) =
      foldl
      (\(b, n) y ->
         if isFull b y
         then ( adjustRow b y (\_ -> Nothing),
                n+1)
         else ( adjustRow b y (\block -> case block of
                                   Just block -> Just (shiftDownN block Speed.rowFall n) 
                                   Nothing -> Nothing
                              ),
                n)
      ) 
      (board, 0)
      [0..21]

--aktualizuje planszę: usuwa pełne rzędy, przesuwa (zaczyna animację) tych wyżej, przeindeksowuje bloki
clearRows :: Board -> Board
clearRows board =
  foldl
  (\Board {board = b} (k, v) ->
     Board {board = Data.HashMap.Strict.insert k v b} )
  Board.empty
  nonEmptyBlocks 
  where
    allBlocks =
      Prelude.map
      (\(_, block) ->
         case block of
           Just block -> (Block.gridPosition block, Just block)
           Nothing -> ((-1, -1), Nothing))
      (Data.HashMap.Strict.toList (Board.board (deleteRows board)))
    nonEmptyBlocks = 
      (Prelude.filter
        (\(_, b) -> case b of
                      Nothing -> False
                      _ -> True)
        allBlocks)

--animuje ruch bloków co tik (ruszają się po tym, jak usunięty zostanie rząd pod nimi)
move :: Board -> Board
move board =
  Board {board = board'}
  where
    board' =
      Data.HashMap.Strict.map
      (\b -> case b of
               Just b -> Just (Block.move b)
               Nothing -> Nothing)
      (Board.board board)
     
