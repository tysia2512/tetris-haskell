module Colors (red, yellow, blue, purple, green, dark_blue, orange) where

import Graphics.UI.GLUT

red :: Color3 GLfloat
red = Color3 1.0 0.0 0.0

yellow :: Color3 GLfloat
yellow = Color3 0.9 1.0 0.0 

blue :: Color3 GLfloat
blue = Color3 0.0 1.0 1.0

purple :: Color3 GLfloat
purple = Color3 0.4 0.0 0.4
  
green :: Color3 GLfloat
green = Color3 0.0 0.8 0.0
  
dark_blue :: Color3 GLfloat
dark_blue = Color3 0.0 0.0 0.4

orange :: Color3 GLfloat
orange = Color3 0.9 0.4 0.0
