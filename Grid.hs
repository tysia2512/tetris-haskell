module Grid ( xEdgeLength,
              yEdgeLength,
              displayedPosition,
              grid,
              vertex3 ) where
import Graphics.UI.GLUT

--rzeczywista (podawana modułowi Graphics.UI.GLUT) długość jednej kratki po OX
xEdgeLength :: Float
xEdgeLength = 0.2

--rzeczywista (podawana modułowi Graphics.UI.GLUT) długość jednej kratki po OY
yEdgeLength :: Float
yEdgeLength = 1.0/11.0

--konwersja współrzędnych planszy na współrzędne dla Graphics.UI.GLUT
--trzyma współrzędne lewego dolnego rogu bloku
displayedPosition :: (Int, Int) -> (Float, Float)
displayedPosition (x, y) = (fromIntegral x * xEdgeLength - 1.0, fromIntegral y * yEdgeLength - 1.0)

vertex3 :: (GLfloat, GLfloat, GLfloat) -> IO ()
vertex3 (x, y, z) = vertex $ Vertex3 x y z       

vertical :: [(GLfloat, GLfloat, GLfloat)]
vertical = [(-1.0 + k / 5 , d, 0.0) | k <- [0..10], d <- [-1.0, 1.0]]

horizontal :: [(GLfloat, GLfloat, GLfloat)]
horizontal = [(d, -1 + (k / 11.0), 0.0) | k <- [0..22], d <- [-1.0, 1.0]]  

--rysuje grid w tym danym kolorze
grid :: Color3 GLfloat -> IO ()
grid color =
  renderPrimitive Lines $ do
  Graphics.UI.GLUT.color $ color
  mapM_ vertex3 (horizontal ++ vertical)
