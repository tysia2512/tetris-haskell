module Speed where

--domyślnym poziomem trudności jest 2
{-
skompilowane na windowsie działa szybciej:
prędkość 10 jest praktycznie niegrywalna,
prędkość 1 jest trudna
-}
levelSpeed :: String -> Float
levelSpeed s =
  case s of
    "1" -> 0.005
    "2" -> 0.01
    "3" -> 0.015
    "4" -> 0.02
    "5" -> 0.025
    "6" -> 0.03
    "7" -> 0.035
    "8" -> 0.04
    "9" -> 0.045
    "10" -> 0.5
    _ -> 0.01

sideShift :: Float
sideShift = 0.9 

pushDown :: Float
pushDown = 0.9

rowFall :: Float
rowFall = 0.01
