module Bindings where
 
import Graphics.UI.GLUT
import Data.IORef

import Tetromino
import Board
import Speed
 
keyboardMouse :: IORef Board -> IORef Tetromino -> KeyboardMouseCallback
keyboardMouse boardRef tetrominoRef (SpecialKey KeyLeft)
  Down _modifiers _position = do
  tetromino <- get tetrominoRef
  board <- get boardRef
  (if isCorrect board (Tetromino.shiftLeft tetromino speed)
    then tetrominoRef $~! (flip Tetromino.shiftLeft speed)
    else return ())
  where speed = Speed.sideShift

keyboardMouse boardRef tetrominoRef (SpecialKey KeyRight)
  Down _modifiers _position = do
  tetromino <- get tetrominoRef
  board <- get boardRef
  (if isCorrect board (Tetromino.shiftRight tetromino speed)
    then tetrominoRef $~! (flip Tetromino.shiftRight speed)
    else return ())
  where speed = Speed.sideShift

keyboardMouse boardRef tetrominoRef (SpecialKey KeyUp)
  Down _modifiers _position = do
  tetromino <- get tetrominoRef
  board <- get boardRef
  (if isCorrect board (Tetromino.rotate tetromino)
    then tetrominoRef $~! (Tetromino.rotate)
    else return ())

keyboardMouse boardRef tetrominoRef (SpecialKey KeyDown)
  _state _modifiers _position = do
  tetromino <- get tetrominoRef
  board <- get boardRef
  (if isCorrect board (Tetromino.shiftDown tetromino speed)
    then tetrominoRef $~! (flip Tetromino.shiftDown speed)
    else return ())
  where
    speed = Speed.pushDown


keyboardMouse _boardRef _tetrominoRef _key _state _modifiers _position =
  return ()
