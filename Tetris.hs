import Graphics.UI.GLUT
import System.Environment
import Data.IORef
import Display
import Board
import Tetromino
import Speed
import Bindings

main :: IO ()
main = do
  (_progName, _args) <- getArgsAndInitialize
  initialDisplayMode $= [DoubleBuffered]
  initialWindowSize $= Size 400 880 --przeskalowane 10x22
  arguments <- getArgs
  window <- createWindow "Tetris"

  tetromino <- Tetromino.new
  currentTetromino <- newIORef tetromino 
  board <- newIORef Board.empty

  displayCallback $= display board currentTetromino
  idleCallback $= Just (idle window board currentTetromino (speed arguments))
  keyboardMouseCallback $= Just (keyboardMouse board currentTetromino)
  mainLoop
  where
    speed [] = levelSpeed ""
    speed l = levelSpeed (head l)
