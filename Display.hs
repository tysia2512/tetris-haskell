module Display (Display.display, idle) where
 
import Graphics.UI.GLUT
import Graphics.UI.GLUT.Window
import Data.IORef

import Grid
import Colors
import Tetromino
import Board
import Block

display :: IORef Board -> IORef Tetromino -> DisplayCallback
display boardRef currentTetrominoRef = do
  clear [ColorBuffer]

  --animacja ruchu tetromina
  currentTetrominoRef $~! (Tetromino.move)
  currentTetromino <- get currentTetrominoRef

  --animacja ruchu bloków na planszy
  boardRef $~! (Board.move)
  board <- get boardRef

  grid  (Tetromino.color currentTetromino)
  ios (Prelude.map (Block.display) (Tetromino.blocks currentTetromino))
  ios (Prelude.map (Block.display) (Board.nonEmptyBlocks board))
  swapBuffers
  where
    ios :: [IO a] -> IO ()
    ios [] = return ()
    ios (x:xs) = do x
                    sequence_ xs

newTetromino :: IORef Board -> IORef Tetromino -> IO ()
newTetromino boardRef tetrominoRef = do
  tetromino <- get tetrominoRef
  boardRef $~! (\board -> addTetromino board tetromino)
  newTetromino <- Tetromino.new
  tetrominoRef $~! (\_tetromino -> newTetromino)

idle :: Window -> IORef Board -> IORef Tetromino -> Float -> IdleCallback
idle window boardRef currentTetrominoRef speed = do
  currentTetromino <- get currentTetrominoRef
  board <- get boardRef

  --opadanie tetromina, lub stworzenie nowego
  (if isCorrect board (Tetromino.falling currentTetromino speed)
   then currentTetrominoRef $~! (flip Tetromino.falling speed)
   else newTetromino boardRef currentTetrominoRef)

  --usuwanie pełnych rzędów
  boardRef $~! Board.clearRows

  --warunek na koniec gry - zamyka okno
  currentTetromino <- get currentTetrominoRef
  board <- get boardRef
  (if isCorrect board (Tetromino.falling currentTetromino speed)
   then return ()
   else destroyWindow window)

  postRedisplay Nothing
