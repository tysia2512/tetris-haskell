{-
moduł implementujący aktywne tetromino
-}
module Tetromino ( Tetromino,
                   new,
                   blocks,
                   Tetromino.color,
                   Tetromino.rotate,
                   Tetromino.shiftLeft,
                   Tetromino.shiftRight,
                   Tetromino.shiftDown,
                   Tetromino.isMovingDown,
                   Tetromino.move,
                   falling ) 
where

import Graphics.UI.GLUT
import System.Random

import Colors
import Block
import Grid

data Tetromino = Tetromino { shape :: Char,
                             position :: (Int, Int),
                             rotation :: Int,
                             color :: Color3 GLfloat,
                             blocks :: [Block]} deriving Show

generateBlocks :: Tetromino -> [Block]
generateBlocks Tetromino { shape = shape,
                           Tetromino.position = (x, y),
                           rotation = rotation,
                           Tetromino.color = color} =
  map createBlock coordinates
  where
    --rysują odpowiednio poziomą i pionową linię z trzech klocków ze środkiem w (x, y)
    vertical (x, y) = [(x, y + v) | v <- [-1, 0, 1]]
    horizontal (x, y) = [(x + v, y) | v <- [-1, 0, 1]]

    coordinates =
      case (shape, rotation) of
        ('I', 0) -> [(x + v, y) | v <- [0..3]]
        ('I', 1) -> [(x, y + v) | v <- [-3..0]]
  
        ('O', 0) -> [(x, y), (x+1, y), (x, y-1), (x+1, y-1)]
  
        ('T', 0) -> (x, y-1) : horizontal (x, y)
        ('T', 1) -> (x-1, y) : vertical (x, y) 
        ('T', 2) -> (x, y) : horizontal (x, y-1)
        ('T', 3) -> (x+1, y) : vertical (x, y)

        ('S', 0) -> [(x-1, y-1), (x, y-1), (x, y), (x+1, y)]
        ('S', 1) -> [(x-1, y+1), (x-1, y), (x, y), (x, y-1)]

        ('Z', 0) -> [(x-1, y), (x, y), (x, y-1), (x+1, y-1)]
        ('Z', 1) -> [(x, y-1), (x, y), (x+1, y), (x+1, y+1)]

        ('J', 0) -> (x+1, y-1) : horizontal (x, y)
        ('J', 1) -> (x-1, y-1) : vertical (x, y)
        ('J', 2) -> (x-1, y) : horizontal (x, y-1)
        ('J', 3) -> (x+1, y+1) : vertical (x, y)

        ('L', 0) -> (x-1, y-1) : horizontal (x, y)
        ('L', 1) -> (x-1, y+1) : vertical (x, y)
        ('L', 2) -> (x+1, y) : horizontal (x, y-1)
        ('L', 3) -> (x+1, y-1) : vertical (x, y)
        
    createBlock = Block.create color

create :: Char -> (Int, Int) -> Int -> Color3 GLfloat -> Tetromino
create shape position rotation color =
  Tetromino { shape = shape,
              Tetromino.position = position,
              rotation = rotation,
              Tetromino.color = color,
              blocks = generateBlocks empty }
  where
    empty = Tetromino { shape = shape,
                        Tetromino.position = position,
                        rotation = rotation,
                        Tetromino.color = color,
                        blocks = [] }

new :: IO Tetromino
new =
  do
    n <- randomRIO (0, 6) :: IO Integer
    (shape, color) <- case n of
                        0 -> return ('I', red)
                        1 -> return ('O', yellow)
                        2 -> return ('T', blue)
                        3 -> return ('S', purple)
                        4 -> return ('Z', green)
                        5 -> return ('J', dark_blue)
                        6 -> return ('L', orange)
    position <- if shape == 'I' then return (3, 21) else return (4, 21)
    return (Tetromino.create shape position 0 color)

rotate :: Tetromino -> Tetromino -- ustawia movement na Nothing
rotate Tetromino { shape = shape,
                   Tetromino.position = (x, y),
                   rotation = rotation,
                   Tetromino.color = color } =
  case shape of
    'I' | rotation == 0 -> Tetromino.create shape (x+2, y+1) 1 color
    'I' | rotation == 1 -> Tetromino.create shape (x-2, y-1) 0 color
    s | s == 'T' || s == 'J' || s == 'L'->
        Tetromino.create shape (x, y) ((rotation + 1) `mod` 4) color
    s | s == 'S' || s == 'Z' ->
        Tetromino.create shape (x, y) ((rotation + 1) `mod` 2) color
    _ -> Tetromino.create shape (x, y) rotation color

data Direction = Left | Right | Down

shift :: Tetromino -> Direction -> Float -> Tetromino
shift Tetromino { shape = shape,
                  Tetromino.position = (x, y),
                  rotation = rotation,
                  Tetromino.color = color,
                  Tetromino.blocks = blocks}
  direction speed =
  case direction of
    Tetromino.Left ->
      Tetromino { shape = shape,
                  Tetromino.position = (x-1, y),
                  Tetromino.rotation = rotation,
                  Tetromino.color = color,
                  Tetromino.blocks = Prelude.map
                                     (\b -> Block.shiftLeft b speed)
                                     blocks }
    Tetromino.Right -> 
      Tetromino { shape = shape,
                  Tetromino.position = (x+1, y),
                  Tetromino.rotation = rotation,
                  Tetromino.color = color,
                  Tetromino.blocks = Prelude.map
                                     (\b -> Block.shiftRight b speed)
                                     blocks }

    Tetromino.Down ->
      Tetromino { shape = shape,
                  Tetromino.position = (x, y-1),
                  Tetromino.rotation = rotation,
                  Tetromino.color = color,
                  Tetromino.blocks = Prelude.map
                                     (\b -> Block.shiftDown b speed)
                                     blocks }


shiftLeft :: Tetromino -> Float -> Tetromino
shiftLeft t s = Tetromino.shift t Tetromino.Left s

shiftRight :: Tetromino -> Float -> Tetromino
shiftRight t s = Tetromino.shift t Tetromino.Right s

shiftDown :: Tetromino -> Float -> Tetromino
shiftDown t s = Tetromino.shift t Tetromino.Down s

--wywoływane co tik w idle - animuje spadanie tetromina 
move :: Tetromino -> Tetromino
move Tetromino { shape = shape,
                 Tetromino.position = position,
                 Tetromino.rotation = rotation,
                 Tetromino.color = color,
                 Tetromino.blocks = blocks} =
  Tetromino { shape = shape,
              Tetromino.position = position,
              Tetromino.rotation = rotation,
              Tetromino.color = color,
              Tetromino.blocks = Prelude.map (\b -> Block.move b) blocks}

isMovingDown :: Tetromino -> Bool
isMovingDown Tetromino { Tetromino.blocks = blocks } =
  case Prelude.filter (Block.isMovingDown) blocks
  of
    [] -> False
    _ -> True
  
--utrzymuje tetromino, żeby było spadające
falling :: Tetromino -> Float -> Tetromino
falling tetromino speed =
  if Tetromino.isMovingDown tetromino 
  then tetromino
  else Tetromino.shiftDown tetromino speed
